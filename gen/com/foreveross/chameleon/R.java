/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.foreveross.chameleon;

public final class R {
    public static final class anim {
        public static final int slide_in_left=0x7f040000;
        public static final int slide_in_right=0x7f040001;
        public static final int slide_out_left=0x7f040002;
        public static final int slide_out_right=0x7f040003;
    }
    public static final class attr {
    }
    public static final class color {
        public static final int Black=0x7f070001;
        public static final int ChattingSenderTextColor=0x7f070019;
        public static final int ConversationVoiceTextColor=0x7f07001a;
        public static final int White=0x7f070000;
        public static final int black=0x7f07000e;
        public static final int blue=0x7f07000f;
        /**  灰色 
         */
        public static final int darkgrey=0x7f070011;
        public static final int font_black=0x7f07001e;
        public static final int font_grey=0x7f07001f;
        public static final int gray_list_cell=0x7f07001d;
        /**  <color name="blue">#FF0000FF</color> 
         */
        public static final int green=0x7f070004;
        public static final int grey=0x7f070010;
        /**  亮灰色 
         */
        public static final int groupgrey=0x7f070014;
        public static final int lightgrey=0x7f070013;
        public static final int lightransparent=0x7f070017;
        public static final int maxticket=0x7f07000b;
        public static final int navpage=0x7f07001b;
        public static final int orange=0x7f070006;
        /**  <color name="grey">#D7D4D4</color> 
         */
        public static final int red=0x7f070002;
        /**  亮灰色 
         */
        public static final int searchblack=0x7f070015;
        /**  亮灰色 
         */
        public static final int semitransparent=0x7f070016;
        public static final int shake_info_text=0x7f07001c;
        public static final int textgreen=0x7f07000c;
        /**  黑灰色 
         */
        public static final int toasterro=0x7f070012;
        public static final int trainpricetext=0x7f070008;
        public static final int trainticketinfo=0x7f070009;
        public static final int trainticketinfofield=0x7f07000a;
        public static final int trans=0x7f070007;
        public static final int transparent=0x7f070018;
        public static final int whi=0x7f070003;
        public static final int white=0x7f07000d;
        public static final int yellow=0x7f070005;
    }
    public static final class dimen {
        /**  Default screen margins, per the Android Design guidelines. 
 Example customization of dimensions originally defined in res/values/dimens.xml
         (such as screen margins) for screens with more than 820dp of available width. This
         would include 7" and 10" devices in landscape (~960dp and ~1280dp respectively). 

         Customize dimensions originally defined in res/values/dimens.xml (such as
         screen margins) for sw720dp devices (e.g. 10" tablets) in landscape here.
    
         */
        public static final int activity_horizontal_margin=0x7f090000;
        public static final int activity_vertical_margin=0x7f090001;
    }
    public static final class drawable {
        public static final int app_dealbtn_normal=0x7f020000;
        public static final int app_dealbtn_press=0x7f020001;
        public static final int app_detail_bg=0x7f020002;
        public static final int app_detail_selected=0x7f020003;
        public static final int app_detailbtn_image=0x7f020004;
        public static final int appicon=0x7f020005;
        public static final int arrow=0x7f020006;
        public static final int arrow_dowm=0x7f020007;
        public static final int back_chock=0x7f020008;
        public static final int back_chocks=0x7f020009;
        public static final int blowup=0x7f02000a;
        public static final int chat_voice_record_bg=0x7f02000b;
        public static final int collect_btn=0x7f02000c;
        public static final int collect_btn_down=0x7f02000d;
        public static final int collect_button_clickbg=0x7f02000e;
        public static final int defauit=0x7f02000f;
        public static final int ic_launcher=0x7f020010;
        public static final int icon=0x7f020011;
        public static final int list_next=0x7f020012;
        public static final int list_null_selector=0x7f020013;
        public static final int listview_9_white_top=0x7f020014;
        public static final int listview_bg_bottom=0x7f020015;
        public static final int listview_bg_cell=0x7f020016;
        public static final int listview_bg_gray_bottom=0x7f020017;
        public static final int listview_bg_gray_cell=0x7f020018;
        public static final int listview_bg_top=0x7f020019;
        public static final int listview_click_bottom=0x7f02001a;
        public static final int listview_click_cell=0x7f02001b;
        public static final int listview_click_top=0x7f02001c;
        public static final int listview_corner=0x7f02001d;
        public static final int listview_corner_gray_bottom=0x7f02001e;
        public static final int listview_corner_gray_cell=0x7f02001f;
        public static final int listview_corner_gray_top=0x7f020020;
        public static final int listview_corner_white_bottom=0x7f020021;
        public static final int listview_corner_white_cell=0x7f020022;
        public static final int listview_corner_white_top=0x7f020023;
        public static final int listview_select_corner=0x7f020024;
        public static final int listview_select_gray_bottom=0x7f020025;
        public static final int listview_select_gray_cell=0x7f020026;
        public static final int listview_select_gray_top=0x7f020027;
        public static final int listview_select_white_bottom=0x7f020028;
        public static final int listview_select_white_cell=0x7f020029;
        public static final int listview_select_white_top=0x7f02002a;
        public static final int login_chat_voice_record_bg=0x7f02002b;
        public static final int msg_title=0x7f02002c;
        public static final int normal_button=0x7f02002d;
        public static final int normal_button_click=0x7f02002e;
        public static final int normal_button_clickbg=0x7f02002f;
        public static final int pad_splash=0x7f020030;
        public static final int radius_bg=0x7f020031;
        public static final int screen=0x7f020032;
        public static final int scroll_content_backgorund=0x7f020033;
        public static final int setting_about=0x7f020034;
        public static final int setting_button=0x7f020035;
        public static final int setting_update=0x7f020036;
        public static final int shadow=0x7f020037;
        public static final int splash_bg=0x7f020038;
        public static final int tab_line=0x7f020039;
        public static final int update_icon=0x7f02003a;
        public static final int web_close=0x7f02003b;
        public static final int welcome=0x7f02003c;
        public static final int white=0x7f02003d;
    }
    public static final class id {
        public static final int ProgressBar01=0x7f0c0047;
        public static final int about_device=0x7f0c000e;
        public static final int about_version=0x7f0c000d;
        public static final int action_settings=0x7f0c0059;
        public static final int allselected=0x7f0c0035;
        public static final int app_dealbtn=0x7f0c0017;
        public static final int autodownloadlayout=0x7f0c0049;
        public static final int btn_install=0x7f0c0009;
        public static final int btn_installList=0x7f0c0003;
        public static final int btn_mainList=0x7f0c0002;
        public static final int btn_sync=0x7f0c0001;
        public static final int btn_uninstallList=0x7f0c0004;
        public static final int btn_unstall=0x7f0c000a;
        public static final int btn_updatableList=0x7f0c0005;
        public static final int button2=0x7f0c000b;
        public static final int chatroom_collect_friend_icon=0x7f0c002b;
        public static final int components_main_mCordovaWebView=0x7f0c0026;
        public static final int components_main_viewContain=0x7f0c0025;
        public static final int components_pad_layouts=0x7f0c004b;
        public static final int delete=0x7f0c0037;
        public static final int delete_icon=0x7f0c0044;
        public static final int delete_msg_layout=0x7f0c0042;
        public static final int detail_img=0x7f0c001b;
        public static final int detail_loadingBar=0x7f0c001e;
        public static final int detail_loadingImglayout=0x7f0c001c;
        public static final int detail_loadtext=0x7f0c001d;
        public static final int dialog_text=0x7f0c002e;
        public static final int editcheckbox=0x7f0c0034;
        public static final int icon=0x7f0c0010;
        public static final int imageView_updata=0x7f0c0011;
        public static final int img_about=0x7f0c0055;
        public static final int img_update=0x7f0c0053;
        public static final int instant_status=0x7f0c0022;
        public static final int listview_installList=0x7f0c0007;
        public static final int listview_uninstallList=0x7f0c0006;
        public static final int listview_updatableList=0x7f0c0008;
        public static final int logoff=0x7f0c0057;
        public static final int mark=0x7f0c0036;
        public static final int mask_before_web_loaded=0x7f0c004e;
        public static final int message_inner_fragment=0x7f0c0000;
        public static final int mi_layout2=0x7f0c0013;
        public static final int mi_layout3=0x7f0c0016;
        public static final int mina_status=0x7f0c001f;
        public static final int module_setting_layout=0x7f0c0056;
        public static final int moreitem_layout=0x7f0c000f;
        public static final int msg_content=0x7f0c003d;
        public static final int msg_icon=0x7f0c003f;
        public static final int msg_num=0x7f0c0043;
        public static final int msg_num_layout=0x7f0c0041;
        public static final int msg_readStatus=0x7f0c003e;
        public static final int msg_sort=0x7f0c0040;
        public static final int msg_time=0x7f0c003c;
        public static final int msg_title=0x7f0c003b;
        public static final int msgbody=0x7f0c0039;
        public static final int msgcheckbox=0x7f0c0038;
        public static final int msglist=0x7f0c0033;
        public static final int name=0x7f0c0014;
        public static final int new_progressBar=0x7f0c002c;
        public static final int pad_splash_screen=0x7f0c0045;
        public static final int pad_splash_screen_import=0x7f0c0032;
        public static final int pad_splash_screen_stub=0x7f0c0031;
        public static final int page_main_content_window_id=0x7f0c0048;
        public static final int page_main_detail_frame_layout=0x7f0c004d;
        public static final int page_main_frament=0x7f0c004c;
        public static final int page_navigation_layoutMain=0x7f0c004f;
        public static final int phone_icon=0x7f0c0051;
        public static final int phone_splash_screen=0x7f0c0050;
        public static final int phone_splash_screen_import=0x7f0c0030;
        public static final int phone_splash_screen_stub=0x7f0c002f;
        public static final int point=0x7f0c001a;
        public static final int progress=0x7f0c004a;
        public static final int progressBar1=0x7f0c002d;
        public static final int progressBar_download=0x7f0c0012;
        public static final int pushsetting_cb_mina=0x7f0c0021;
        public static final int pushsetting_cb_xmpp=0x7f0c0024;
        public static final int pushsetting_cbstatus_mina=0x7f0c0020;
        public static final int pushsetting_cbstatus_xmpp=0x7f0c0023;
        public static final int relativeLayout1=0x7f0c0027;
        public static final int relativeLayout1_ref=0x7f0c000c;
        public static final int releaseNote=0x7f0c0018;
        public static final int setting_btn_about=0x7f0c0054;
        public static final int setting_btn_update=0x7f0c0052;
        public static final int slidePageView=0x7f0c0019;
        public static final int titlePanel=0x7f0c003a;
        public static final int title_barcontent=0x7f0c0029;
        public static final int title_barleft=0x7f0c0028;
        public static final int title_barright=0x7f0c002a;
        public static final int version=0x7f0c0015;
        public static final int web_close_button=0x7f0c0058;
        public static final int welcome_loadinglayout=0x7f0c0046;
    }
    public static final class layout {
        public static final int activity_app_detail=0x7f030000;
        public static final int activity_cube_module_main=0x7f030001;
        public static final int activity_main=0x7f030002;
        public static final int activity_operation=0x7f030003;
        public static final int app_about=0x7f030004;
        public static final int app_detail=0x7f030005;
        public static final int app_detail_item=0x7f030006;
        public static final int app_pushsetting=0x7f030007;
        public static final int components_main=0x7f030008;
        public static final int cube_titlebar=0x7f030009;
        public static final int dialog_layout=0x7f03000a;
        public static final int imm_main=0x7f03000b;
        public static final int loading=0x7f03000c;
        public static final int message=0x7f03000d;
        public static final int message_activity_layout=0x7f03000e;
        public static final int msg_content=0x7f03000f;
        public static final int msg_title=0x7f030010;
        public static final int pad_splash_screen=0x7f030011;
        public static final int page_main=0x7f030012;
        public static final int page_navigation=0x7f030013;
        public static final int phone_splash_screen=0x7f030014;
        public static final int setting=0x7f030015;
        public static final int web_close=0x7f030016;
    }
    public static final class menu {
        public static final int app_detail=0x7f0b0000;
        public static final int cube_module_main=0x7f0b0001;
        public static final int main=0x7f0b0002;
        public static final int operation=0x7f0b0003;
    }
    public static final class raw {
        public static final int cube=0x7f060000;
        public static final int cube_mdm=0x7f060001;
        public static final int cube_test=0x7f060002;
        public static final int pushmodule=0x7f060003;
    }
    public static final class string {
        public static final int activity_name=0x7f0a0002;
        public static final int app_name=0x7f0a0000;
        public static final int crash_dialog_text=0x7f0a0007;
        public static final int crash_dialog_title=0x7f0a0006;
        public static final int crash_no=0x7f0a0009;
        public static final int crash_notif_text=0x7f0a0005;
        public static final int crash_notif_ticker_text=0x7f0a0003;
        public static final int crash_notif_title=0x7f0a0004;
        public static final int crash_subject=0x7f0a000a;
        public static final int crash_yes=0x7f0a0008;
        public static final int launcher_name=0x7f0a0001;
    }
    public static final class style {
        /** 
        Base application theme, dependent on API level. This theme is replaced
        by AppBaseTheme from res/values-vXX/styles.xml on newer devices.
    

            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.
        

        Base application theme for API 11+. This theme completely replaces
        AppBaseTheme from res/values/styles.xml on API 11+ devices.
    
 API 11 theme customizations can go here. 

        Base application theme for API 14+. This theme completely replaces
        AppBaseTheme from BOTH res/values/styles.xml and
        res/values-v11/styles.xml on API 14+ devices.
    
 API 14 theme customizations can go here. 
         */
        public static final int AppBaseTheme=0x7f080003;
        /**  Application theme. 
 All customizations that are NOT specific to a particular API-level can go here. 
         */
        public static final int AppTheme=0x7f080004;
        /**  背景阴影 
         */
        public static final int common_dialog=0x7f080002;
        /**  背景阴影 
         */
        public static final int dialog=0x7f080005;
        /**  背景阴影 
         */
        public static final int menu_dialog=0x7f080000;
        /**  背景阴影 
         */
        public static final int skin_dialog=0x7f080006;
        /**  背景阴影 
         */
        public static final int voice_dialog=0x7f080001;
    }
    public static final class xml {
        public static final int config=0x7f050000;
        public static final int mdm=0x7f050001;
    }
}
