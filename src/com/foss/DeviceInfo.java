package com.foss;
import android.app.Application;
import android.content.Context;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;

import java.text.DecimalFormat;
/**
 * project:
 * author: wzq
 * date: 2014/7/21
 * description: 手机信息，包括硬件信息和软件信息等 , 只读常量
 */
public final class DeviceInfo {

    public static final int sdkVer;//SDK 版本号
    public static final String release;

    public static final int screenWidthPixel;//屏幕宽度
    public static final int screenHeightPixel;//屏幕高度
    public static final float xdpi;
    public static final float ydpi;
    public static final float density;//屏幕密度
    public static final float widthInch;//屏幕inch
    public static final float heightInch;//屏幕inch
    public static final float dialogInch;//对角尺寸
    public static final String manufacture;//生产商
    public static final String model;//设备型号
    public static final String imei;//IMEI

    private DeviceInfo() {}
    static {
        Application application = ChameleonApplication.getApplication();
        sdkVer = android.os.Build.VERSION.SDK_INT;
        release = android.os.Build.VERSION.RELEASE;

        manufacture = android.os.Build.MANUFACTURER;
        model = android.os.Build.MODEL;

        DisplayMetrics dm = application.getResources().getDisplayMetrics();
        screenWidthPixel = dm.widthPixels;
        screenHeightPixel = dm.heightPixels;
        density = dm.densityDpi;
        // TODO:华为U8860测试不准确 why?
        xdpi = dm.xdpi;
        ydpi = dm.ydpi;
        DecimalFormat ff = new DecimalFormat("#.##");
        widthInch = Float.parseFloat(ff.format((float) screenWidthPixel / xdpi));
        heightInch = Float.parseFloat(ff.format((float) screenHeightPixel / ydpi));
        dialogInch = Float.parseFloat(ff.format((float) Math.hypot(widthInch, heightInch)));

        // IMEI
        TelephonyManager tm = (TelephonyManager) application.getSystemService(Context.TELEPHONY_SERVICE);
        imei = tm.getDeviceId();

    }
}
