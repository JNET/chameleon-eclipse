package com.foss;
import android.app.Application;
import com.foreveross.bsl.util.Preferences;
import com.foreveross.chameleonsdk.config.URL;
/**
 * @author: wzq
 * @date: 14-6-12
 * description: change it at File | setting | File and code templates | include | file header
 */
public class ChameleonApplication extends Application {

    private static Application application;

    @Override
    public void onCreate() {
        super.onCreate();
        System.out.println(" ChameleonApplication onCreate ");
        application = this;

        init();
    }

    public static Application getApplication() {
        if(application == null) {
            throw new RuntimeException("必须继承ChameleonApplication");
        }
        return application;
    }

    // 初始化
    public static void init() {
        URL.init(getApplication());
        Preferences.getInstance(getApplication());
    }
}
