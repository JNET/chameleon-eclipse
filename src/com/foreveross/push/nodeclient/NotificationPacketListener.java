package com.foreveross.push.nodeclient;

import org.jivesoftware.smack.packet.Packet;

import java.util.ArrayList;
import java.util.List;

public class NotificationPacketListener{
	
	public static List<BaseParser> parses = new ArrayList<BaseParser>();

	public static void processPacket(Packet packet) {
		for(BaseParser parse:parses){
			parse.onReceive(packet);
		}
	}
	
	public static void register(BaseParser parser){
		parses.add(parser);
	}
}
