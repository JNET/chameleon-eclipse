package com.foreveross.chameleon_template;

import java.io.File;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.foreveross.bsl.util.FileCopeTool;
import com.foreveross.bsl.util.Preferences;
import com.foreveross.bsl.util.ZipUtils;
import com.foreveross.chameleon.R;
import com.foreveross.chameleonsdk.CModule;
import com.foreveross.chameleonsdk.config.URL;
import com.foss.DeviceInfoUtil;
import com.foss.GeneralUtils;

public class MainActivity extends Activity {
    private TemplateApplication temphetApplication;
    private RelativeLayout layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(DeviceInfoUtil.isPad()) {
            setContentView(R.layout.pad_splash_screen);
        } else {
            setContentView(R.layout.phone_splash_screen);
        }
        temphetApplication = TemplateApplication.class.cast(MainActivity.this.getApplication());
        layout = (RelativeLayout) findViewById(R.id.welcome_loadinglayout);

        String basePath = Environment.getExternalStorageDirectory().getPath() + "/" + URL.APP_PACKAGENAME;

        //应用是否第一次使用
        Boolean isFirstTime = Preferences.getFirsttime();
        //www.zip是否已经被解压缩
        boolean hasUnZip = true;
        //www文件夹是否存在于手机sdcard内
        File file = new File(basePath + "/www");
        if(!file.exists()) {
            hasUnZip = false;
        }

        if(isFirstTime) {
            //第一次使用，必须解压www.zip
            Preferences.saveFirsttime(false);
            hasUnZip = false;
            Preferences.saveVersionCode(GeneralUtils.getVersionCode());
        } else {
            int versionCode = Preferences.getVersionCode();
            if(versionCode < GeneralUtils.getVersionCode()) {
                //升级应用后，重新解压www.zip覆盖旧文件
                Preferences.saveVersionCode(GeneralUtils.getVersionCode());
                hasUnZip = false;
            }
        }

        if(!hasUnZip) {
            //符合解压条件，开始解压
            file.mkdirs();
            UnZipAsynTask unZipAsynTask = new UnZipAsynTask(MainActivity.this);
            //unZipAsynTask.execute("www/www.zip", basePath + "/www/www.zip", basePath + "/www");
            unZipAsynTask.execute("www/www.zip",basePath + "/www/www.zip",basePath);
        } else {
            init();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void goToFirstModule() {
        HashMap<String, CModule> modules = temphetApplication.getCApplication().getModules();
        for(CModule module : modules.values()) {
            String activity = module.getActivity();
            if(module.isFirstStart()) {
                Intent intent = new Intent();
                intent.setClassName(this, activity);
                this.startActivity(intent);
                finish();
            }
        }
    }


    /*
     * 初始化方法实现
     */
    private void init() {
        // 启动加载页面方式：一
        final long currentStart = System.currentTimeMillis();
        new Thread(new Runnable() {

            public void run() {
                // 休眠标识
                boolean flag = false;
                while(!flag) {
                    // 如果休眠时间小于两秒，继续休眠
                    if(System.currentTimeMillis() - currentStart > 1000) {
                        flag = true;
                        goToFirstModule();
                    }
                }
            }
        }).start();
    }

    class UnZipAsynTask extends AsyncTask<String, Integer, Boolean> {
        protected Context context;
        public UnZipAsynTask(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            layout.setVisibility(View.VISIBLE);
            System.out.println(System.currentTimeMillis() + "");
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if(result) {
                System.out.println(System.currentTimeMillis() + "");
                layout.setVisibility(View.GONE);
                Preferences.saveFirsttime(false);
                goToFirstModule();
            } else {
                Toast.makeText(MainActivity.this, "初始化失败", Toast.LENGTH_SHORT).show();
                finish();
            }
        }

        @Override
        protected Boolean doInBackground(String... params) {
            boolean result = false;
            try {
                FileCopeTool tool = new FileCopeTool(context);
                Log.e("MainActivity","\nparams[0]" + params[0]+"\nparams[1]"+params[1]);
                boolean copySuccess = tool.CopyAssetsFile(params[0], params[1]);
                Log.e("MainActivity","copySuccess==>" + copySuccess);
                if(copySuccess) {
                    if(ZipUtils.unZipFile(params[1], params[2])) {
                        result = true;
                    }
                }
            } catch(Exception e) {
                result = true;
                e.printStackTrace();
            }
            return result;
        }
    }
}
