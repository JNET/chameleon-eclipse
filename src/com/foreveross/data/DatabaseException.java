package com.foreveross.data;

/**
 * 自定义数据库异常
 * @author kuangsunny
 *
 */
public class DatabaseException extends Exception {

	private static final long serialVersionUID = 1396155837630180169L;

	public DatabaseException(Exception e) {
		super(e);
	}
}
