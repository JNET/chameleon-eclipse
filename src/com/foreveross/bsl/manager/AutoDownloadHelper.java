package com.foreveross.bsl.manager;

import java.util.HashMap;
import java.util.Map;

import com.foreveross.bsl.model.CubeModule;


/**
 * @author Amberlo
 * @2013-12-31	
 * @CubeModuleManager
 * [功能描述]自动下载功能助手
 */
public class AutoDownloadHelper {

	private static AutoDownloadHelper instance;
	
	//当前自动下载的任务列表
	private static Map<String,CubeModule> downloadList = new HashMap<String, CubeModule>();
	
	//当前自动下载的任务列表
	private static Map<String,CubeModule> totalList = new HashMap<String, CubeModule>();
	
//	//自动下载的总数
//	private static int autoDownloadCount = 0;
	
	
	
	

	/**
	 * @return
	 */
	public static AutoDownloadHelper getInstance(){
		if(instance ==null){
			instance = new AutoDownloadHelper();
		}
		return instance;
	}
	
//	public static int getAutoDownloadCount() {
//		return autoDownloadCount;
//	}
//	
//	//设置自动下载任务的总数
//	public void setAutoDownloadCount(int autoDownloadCount) {
//		AutoDownloadHelper.autoDownloadCount = autoDownloadCount;
//	}
	
	public int getProgressCount(){
		return downloadList.size();
	}
	
	public int getTotalCount(){
		return totalList.size();
	}
	
	public boolean addDownloadTask(CubeModule module){
		boolean isSuccess = false;
		if(!downloadList.containsKey(module.getIdentifier())){
			synchronized (downloadList) {
				downloadList.put( module.getIdentifier() , module);
				isSuccess=true;
			}
		}else{
			isSuccess=false;
		}
		
		if(!totalList.containsKey(module.getIdentifier())){
			synchronized (totalList) {
				totalList.put( module.getIdentifier() , module);
				isSuccess=true;
			}
		}else{
			isSuccess=false;
		}
		return isSuccess;
	}
	
	public void finishDownload(CubeModule module){
		if(downloadList.containsKey(module.getIdentifier())){
			synchronized (downloadList) {
				downloadList.remove(module.getIdentifier());
			}
		}
		
	}
	
	public void clear(){
		synchronized (downloadList) {
			downloadList.clear();
//			autoDownloadCount=0;
			totalList.clear();
		}
	}
}
