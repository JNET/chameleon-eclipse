package com.foreveross.bsl.manager;

public interface ApplicationSyncListener {

	public void syncStart();
	
	public void syncFinish();
	
	public void syncFail();
	
	public void syncFinish(String result);
}
