package com.foreveross.bsl.plugin;

import java.util.ArrayList;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;

import android.content.Intent;
import android.text.BoringLayout;

import com.foreveross.bsl.model.UserPrivilege;
import com.foreveross.bsl.util.BroadcastConstans;
import com.google.gson.Gson;

public class PrivilegePlugin extends CordovaPlugin{

	@Override
	public boolean execute(String action, JSONArray args,CallbackContext callbackContext) throws JSONException {
		String result = null ;
		if(action.equals("getPrivileges")) {
			
			result = new Gson().toJson(getPrivilege());
		}
		if (result != null) {
			System.out.println("Privilege = "+result);
			callbackContext.success(result);
			
		}
		return super.execute(action, args, callbackContext);
	}
	
	public ArrayList<String > getPrivilege() {
		ArrayList<String > list = UserPrivilege.getInstance().getPrivilegeList();
		return list;
	}
}
