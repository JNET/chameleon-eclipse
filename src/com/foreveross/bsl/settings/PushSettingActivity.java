package com.foreveross.bsl.settings;

import com.foreveross.chameleon.R;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
//import com.foreveross.chameleon.manager.R;

public class PushSettingActivity extends Activity {

    private LinearLayout titlebar_left;
    private Button titlebar_right;
    private TextView titlebar_content;

    private TextView cbStatusXmpp;
    private CheckBox xmppCheckBox;

    private TextView cbStatusMina;
    private CheckBox minaCheckBox;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app_pushsetting);
        initValue();
    }
    private void initValue() {
        titlebar_left = (LinearLayout) findViewById(R.id.title_barleft);
        titlebar_left.setOnClickListener(clickListener);
        titlebar_right = (Button) findViewById(R.id.title_barright);
        titlebar_right.setVisibility(View.GONE);
        titlebar_content = (TextView) findViewById(R.id.title_barcontent);
        titlebar_content.setText("推送设置");

        cbStatusMina = (TextView) findViewById(R.id.pushsetting_cbstatus_mina);
        cbStatusXmpp = (TextView) findViewById(R.id.pushsetting_cbstatus_xmpp);
        xmppCheckBox = (CheckBox) findViewById(R.id.pushsetting_cb_xmpp);
        xmppCheckBox.setClickable(false);
        xmppCheckBox.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    OnClickListener clickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            if(v.getId() == R.id.title_barleft) {
                finish();
            }
        }
    };

    /**
     * [一句话功能简述]<BR>
     * [功能详细描述] 2013-10-19 上午11:36:05
     */
    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
    }

}
